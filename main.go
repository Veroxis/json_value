package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"value_type/value"
)

func main() {
	err := run()
	if err != nil {
		panic(err)
	}
}

func run() error {
	jsonFile, err := os.Open("data.json")
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return err
	}

	val, err := value.ValueFromJsonBytes(byteValue)
	if err != nil {
		return err
	}

	fmt.Println(val.AsArray(nil))
	fmt.Println(val.AsBool(nil))
	fmt.Println(val.AsFloat(nil))
	fmt.Println(val.AsInt(nil))
	fmt.Println(val.AsMap(nil))
	fmt.Println(val.AsString(nil))

	indexedStringVal, err := val.AsValue([]interface{}{0, "tags", 0})
	if err != nil {
		return err
	}

	fmt.Println()
	fmt.Println(indexedStringVal.AsArray(nil))
	fmt.Println(indexedStringVal.AsBool(nil))
	fmt.Println(indexedStringVal.AsFloat(nil))
	fmt.Println(indexedStringVal.AsInt(nil))
	fmt.Println(indexedStringVal.AsMap(nil))
	fmt.Println(indexedStringVal.AsString(nil))

	indexedFloatVal, err := val.AsValue([]interface{}{0, "latitude"})
	if err != nil {
		return err
	}

	fmt.Println()
	fmt.Println(indexedFloatVal.AsArray(nil))
	fmt.Println(indexedFloatVal.AsBool(nil))
	fmt.Println(indexedFloatVal.AsFloat(nil))
	fmt.Println(indexedFloatVal.AsInt(nil))
	fmt.Println(indexedFloatVal.AsMap(nil))
	fmt.Println(indexedFloatVal.AsString(nil))

	// final api usage
	fmt.Println()
	fmt.Println(val.AsValue(nil))
	fmt.Println(val.AsValue([]interface{}{}))
	fmt.Println(val.AsValue([]interface{}{0}))
	fmt.Println(val.AsMap([]interface{}{0}))
	fmt.Println(val.AsArray([]interface{}{0, "tags"}))
	fmt.Println(val.AsString([]interface{}{0, "tags", 0}))
	fmt.Println(val.AsFloat([]interface{}{0, "latitude"}))

	return nil
}
