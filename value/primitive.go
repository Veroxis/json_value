package value

import (
	"encoding/json"
	"fmt"
)

type Value struct {
	inner interface{}
}

func ValueFromJsonString(data string) (*Value, error) {
	return ValueFromJsonBytes([]byte(data))
}

func ValueFromJsonBytes(data []byte) (*Value, error) {
	var valueRaw interface{}
	err := json.Unmarshal(data, &valueRaw)
	if err != nil {
		return nil, err
	}

	value, err := ValueFromUnmarshalled(valueRaw)
	if err != nil {
		return nil, err
	}

	return value, nil
}

func ValueFromUnmarshalled(data interface{}) (*Value, error) {
	_, ok := data.(string)
	if ok {
		return &Value{inner: data}, nil
	}
	_, ok = data.(int)
	if ok {
		return &Value{inner: data}, nil
	}
	_, ok = data.(float64)
	if ok {
		return &Value{inner: data}, nil
	}
	_, ok = data.(bool)
	if ok {
		return &Value{inner: data}, nil
	}
	valMapRaw, ok := data.(map[string]interface{})
	if ok {
		valMap := make(map[string]*Value)
		for key, val := range valMapRaw {
			innerVal, err := ValueFromUnmarshalled(val)
			if err != err {
				return nil, err
			}
			valMap[key] = innerVal
		}
		return &Value{inner: valMap}, nil
	}
	varArrayRaw, ok := data.([]interface{})
	if ok {
		var valArray []*Value
		for _, val := range varArrayRaw {
			innerVal, err := ValueFromUnmarshalled(val)
			if err != err {
				return nil, err
			}
			valArray = append(valArray, innerVal)
		}
		return &Value{inner: valArray}, nil
	}
	return nil, fmt.Errorf("unknown value type")
}

func (v *Value) AsString(idx []interface{}) (string, error) {
	value, err := v.AsValue(idx)
	if err != nil {
		return "", err
	}
	val, ok := value.inner.(string)
	if !ok {
		return "", fmt.Errorf("failed to convert Value to `string`")
	}
	return val, nil
}

func (v *Value) AsInt(idx []interface{}) (int, error) {
	value, err := v.AsValue(idx)
	if err != nil {
		return 0, err
	}
	val, ok := value.inner.(int)
	if !ok {
		return 0, fmt.Errorf("failed to convert Value to `int`")
	}
	return val, nil
}

func (v *Value) AsFloat(idx []interface{}) (float64, error) {
	value, err := v.AsValue(idx)
	if err != nil {
		return 0, err
	}
	val, ok := value.inner.(float64)
	if !ok {
		return 0, fmt.Errorf("failed to convert Value to `float64`")
	}
	return val, nil
}

func (v *Value) AsBool(idx []interface{}) (bool, error) {
	value, err := v.AsValue(idx)
	if err != nil {
		return false, err
	}
	val, ok := value.inner.(bool)
	if !ok {
		return false, fmt.Errorf("failed to convert Value to `bool`")
	}
	return val, nil
}

func (v *Value) AsMap(idx []interface{}) (map[string]*Value, error) {
	value, err := v.AsValue(idx)
	if err != nil {
		return nil, err
	}
	valueMap, ok := value.inner.(map[string]*Value)
	if !ok {
		return nil, fmt.Errorf("failed to convert Value %v to `map[string]interface{}`", v.inner)
	}
	return valueMap, nil
}

func (v *Value) AsArray(idx []interface{}) ([]*Value, error) {
	value, err := v.AsValue(idx)
	if err != nil {
		return nil, err
	}
	val, ok := value.inner.([]*Value)
	if !ok {
		return nil, fmt.Errorf("failed to convert Value to `[]*Value`")
	}
	return val, nil
}

func (v *Value) AsValue(idx []interface{}) (*Value, error) {
	needle := v
	for _, jdx := range idx {
		intJdx, ok := jdx.(int)
		if ok {
			arrNeedle, err := needle.AsArray(nil)
			if err == nil {
				needle = arrNeedle[intJdx]
				continue
			}

			return nil, fmt.Errorf("failed to index Value %v using `int` %v", needle, intJdx)
		}

		stringJdx, ok := jdx.(string)
		if ok {
			mapNeedle, err := needle.AsMap(nil)
			if err == nil {
				needle = mapNeedle[stringJdx]
				continue
			}
			return nil, fmt.Errorf("failed to index Value using string `%v`", stringJdx)
		}

		return nil, fmt.Errorf("failed to index %v", jdx)
	}
	return needle, nil
}
